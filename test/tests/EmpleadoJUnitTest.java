/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import poo_examenrecuperatorio.Empleado;

/**
 *
 * @author Horacio Chazarreta
 */
public class EmpleadoJUnitTest {
    
    Empleado empleado = new Empleado();
    
    public EmpleadoJUnitTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCalcularSalarioNeto(){
        System.out.println("Prueba calcularSalarioNeto");
        // Prueba 1
        assertEquals(8000, empleado.calcularSalarioNeto(8000),0.01);
        // Prueba 2
        assertEquals(10080, empleado.calcularSalarioNeto(12000),0.01);
        // Prueba 3
        assertEquals(12300, empleado.calcularSalarioNeto(15000),0.01);
        // Prueba 4
        assertEquals(0, empleado.calcularSalarioNeto(-1000),0.01);
        
    }
    
}
