/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_examenrecuperatorio;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Horacio Chazarreta
 */
public class Principal {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String nombreEmp;
        Empresa empresa = null;
        List<Empresa> listaEmp = new ArrayList();
        List<Cliente> listaCli = new ArrayList();
        List<Empleado> listaEmpleado = new ArrayList();
        int opcion;
        try {

            do {
                System.out.println("\n******MENÚ PRINCIPAL******");
                System.out.println("1. Ingresar dato de una empresa");
                System.out.println("2. Listar todas las empresas");
                System.out.println("3. Agregar cliente a empresa");
                System.out.println("4. Agregar empleado a empresa");
                System.out.println("5. Mostrar listado de clientes de una empresa con todos sus datos");
                System.out.println("6. Mostrar empleados de una empresa con todos sus datos junto con el salario bruto y neto");
                System.out.println("7. Salir");
                System.out.println("Ingrese una opción: ");
                opcion = entrada.nextInt();
                entrada.nextLine();
                switch (opcion) {
                    case 1:
                        // Ingresar nombre de la empresa
                        System.out.println("Datos de la empresa");
                        System.out.println("Ingrese nombre de la empresa: ");
                        nombreEmp = entrada.nextLine();
                        empresa = new Empresa(nombreEmp);
                        listaEmp.add(empresa);
                        break;
                    case 2:
                        if (listaEmp.isEmpty()) {
                            System.out.println("No hay empresas para listar");
                        } else {
                            System.out.println("Listado de Empresas");
                            for (Empresa emp : listaEmp) {
                                System.out.println(emp.listarEmpresas());
                            }
                        }
                        break;
                    case 3:
                        // Agregar cliente a empresa
                        // Aclaracion: solo agrega a la ultima empresa que se agrego
                        if (listaEmp.isEmpty()) {
                            System.out.println("La lista esta vacia");
                            System.out.println("Debe agregar una empresa");
                        } else {
                            System.out.print("Ingrese el nombre de la empresa a la que desea agregar el Cliente: ");
                            String nombreEmpA = entrada.nextLine();
                            if (nombreEmpA.equalsIgnoreCase(empresa.getNombreEmpresa())) {
                                System.out.println("El nombre de la empresa no existe");

                                System.out.println("Datos del Cliente");
                                System.out.print("Ingrese DNI: ");
                                String dni = entrada.nextLine();
                                System.out.print("Ingrese Apellido y Nombre: ");
                                String ayn = entrada.nextLine();
                                System.out.print("Ingrese Direccion: ");
                                String direccion = entrada.nextLine();
                                Cliente cliente = new Cliente(dni, ayn, direccion);
                                listaCli.add(cliente);
                                empresa.setClientes(listaCli);
                            }
//                        Empresa emp = new Empresa(clientes);
//                        emp.getClientes();
                            //clientes = (List<Cliente>) new Cliente(dni, ayn, direccion);
                            //empresas.add((Empresa) clientes);
                        }
                        break;
                    case 4:
                        // Agregar empleado a empresa
                        // Aclaracion: solo agrega a la ultima empresa que se agrego
                        if (listaEmp.isEmpty()) {
                            System.out.println("La lista esta vacia");
                            System.out.println("Debe agregar una empresa");
                        } else {
                            System.out.print("Ingrese el nombre de la empresa a la que desea agregar el Empleado: ");
                            String nombreEmpA1 = entrada.nextLine();
                            if (nombreEmpA1.equalsIgnoreCase(empresa.getNombreEmpresa())) {
                                System.out.println("Datos del Empleado");
                                System.out.print("Ingrese Codigo: ");
                                int cod = entrada.nextInt();
                                entrada.nextLine();
                                System.out.print("Ingrese Apellido y Nombre: ");
                                String ayn = entrada.nextLine();
                                System.out.print("Ingrese Tipo: ");
                                String tipo = entrada.nextLine();
                                System.out.print("Ingrese Ventas del Mes: ");
                                int ventasMes = entrada.nextInt();
                                entrada.nextLine();
                                System.out.print("Ingrese Horas Extra: ");
                                int horasE = entrada.nextInt();
                                Empleado empleado = new Empleado(cod, ayn, tipo, ventasMes, horasE);
                                listaEmpleado.add(empleado);
                                empresa.setEmpleados(listaEmpleado);
                            }
                        }
                        break;
                    case 5:
                        for (Empresa cliEmp : listaEmp) {
                            System.out.println(cliEmp.listarClientesEmpresas());
                        }
                        break;
                    case 6:
                        for (Empresa empEmpresa : listaEmp) {
                            System.out.println(empEmpresa.listarEmpleadosEmpresas());
                        }
                        break;
                    case 7:
                        System.out.println("Fin del Programa");
                        break;
                    default:
                        System.out.println("Opción Incorrecta");
                        break;
                }
            } while (opcion != 7);
        } catch (InputMismatchException ex) {
            System.out.println("Ingrese un numero");
        }
    }

}
