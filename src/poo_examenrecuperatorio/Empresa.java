/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_examenrecuperatorio;

import java.util.List;

/**
 *
 * @author Horacio Chazarreta
 */
public class Empresa {
    private List <Cliente> clientes;
    private List <Empleado> empleados;
    private String nombreEmpresa;

    public Empresa() {
    }

    public Empresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public Empresa(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String listarEmpresas(){
        return "Nombre de la Empresa: " + nombreEmpresa;
    }
    
    public String listarClientesEmpresas(){
        return "Nombre de la Empresa: " + nombreEmpresa + "\nClientes: " + clientes;
    }
    
    public String listarEmpleadosEmpresas(){
        return "Nombre de la Empresa: " + nombreEmpresa + "\nEmpleados: " + empleados;
    }
    
}
