/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_examenrecuperatorio;

/**
 *
 * @author Horacio Chazarreta
 */
public class Empleado {

    private int codigo;
    private String ayn;
    private String tipo;
    private int ventasMes;
    private int horasExtra;

    public Empleado() {
    }

    public Empleado(int codigo, String ayn, String tipo, int ventasMes, int horasExtra) {
        this.codigo = codigo;
        this.ayn = ayn;
        this.tipo = tipo;
        this.ventasMes = ventasMes;
        this.horasExtra = horasExtra;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getVentasMes() {
        return ventasMes;
    }

    public void setVentasMes(int ventasMes) {
        this.ventasMes = ventasMes;
    }

    public int getHorasExtra() {
        return horasExtra;
    }

    public void setHorasExtra(int horasExtra) {
        this.horasExtra = horasExtra;
    }

    @Override
    public String toString() {
        return "Empleado{" + "codigo=" + codigo + ", ayn=" + ayn + ", tipo=" + tipo
                + ", ventasMes=" + ventasMes + ", horasExtra=" + horasExtra
                + ", salarioBruto=" + calcularSalarioBruto(tipo, ventasMes, horasExtra)
                + ", salarioNeto=" + calcularSalarioNeto(calcularSalarioBruto(tipo, ventasMes, horasExtra)) + '}';
    }

    public int calcularSalarioBruto(String tipo, int ventasMes, int horasExtra) {
        int salarioBruto = 0;
        if (tipo.equalsIgnoreCase("Vendedor")) {
            salarioBruto = 10000;
            if (ventasMes >= 10000) {
                salarioBruto += 1000;
            } else if (ventasMes >= 15000) {
                salarioBruto += 1500;
            }
            salarioBruto += horasExtra * 200;
        } else if (tipo.equalsIgnoreCase("Encargado")) {
            salarioBruto = 15000;
            if (ventasMes >= 10000) {
                salarioBruto += 1000;
            } else if (ventasMes >= 15000) {
                salarioBruto += 1500;
            }
            salarioBruto += horasExtra * 200;
        } else if (ventasMes < 0 || horasExtra < 0) {
            return 0;
        } 
        return salarioBruto;
    }

    public float calcularSalarioNeto(int salarioBruto) {
        float salarioNeto = 0;
        if (salarioBruto < 10000) {
            salarioNeto = salarioBruto;
        } else {
            if (salarioBruto >= 10000 && salarioBruto < 15000) {
                salarioNeto = salarioBruto - salarioBruto * 16 / 100;
            } else {
                if (salarioBruto >= 15000) {
                    salarioNeto = salarioBruto - salarioBruto * 0.18f; // --> linea corregida
                    //salarioNeto = salarioBruto + salarioBruto * 0.18f; --> linea que tenia errores
                }
            }
        }
        if (salarioBruto < 0) {
            return 0;
        } else {
            return salarioNeto;
        }

    }
}
